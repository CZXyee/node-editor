import { BaseLine, LineDefine } from "./interface/index.ts";
import { getID } from "./utils/createId.ts";
import { IDentity } from "../types/propert.ts";

/**
 * 线
 * tailNode --> headNode
 */
class Line implements BaseLine {
    id: IDentity;
    headNodeID;
    tailNodeID;
    headSocketIndex;
    tailSocketIndex;

    constructor({
        headNodeID,
        tailNodeID,
        headSocketIndex,
        tailSocketIndex,
    }: LineDefine) {
        this.id = getID();

        this.headNodeID = headNodeID;
        this.tailNodeID = tailNodeID;
        this.headSocketIndex = headSocketIndex;
        this.tailSocketIndex = tailSocketIndex;
    }
}

export default Line;

