import {
    BaseNode,
    NodeDefine,
    SocketDefines,
    Sockets,
} from "./interface/index.ts";
import { getID } from "./utils/createId.ts";
import { IDentity } from "../types/propert.ts";

/** 节点 */
class Node implements BaseNode {
    title;
    describe;
    sockets;
    nodeFunction() {}

    /** 节点 id */
    id: IDentity;
    /** 出节点的线 */
    outLines: { [id: IDentity]: IDentity } = {};
    /** 进节点的线 */
    inLines: { [id: IDentity]: IDentity } = {};

    /**
     * @param node 节点
     */
    constructor(node: NodeDefine) {
        this.title = node.title;
        this.describe = node.describe;
        this.nodeFunction = node.nodeFunction;

        let socketDefines = structuredClone(node.sockets);
        this.sockets = this.defineSockets(socketDefines);

        this.id = getID();
    }

    /**
     * 定义节点，设置id
     * @param socketDefines 节点定义对象
     */
    private defineSockets(socketDefines: SocketDefines): Sockets {
        for (let id in socketDefines) {
            socketDefines[id] = Object.assign(socketDefines[id], { id: id });
        }
        let sockets = socketDefines as Sockets;
        return sockets;
    }
}

export default Node;
