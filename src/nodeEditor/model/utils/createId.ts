import { IDentity } from "../../types/propert";

/**
 * 生成一个 id
 */
const getID = (): IDentity => {
    let data = Date.now() + "";
    let random = Math.round(Math.random() * 1000);
    return data + "-" + random;
};

export { getID };

