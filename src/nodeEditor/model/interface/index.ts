export type { BaseNode, BaseLine, Socket, Sockets } from "./style.ts";
export type {
    NodeDefine,
    LineDefine,
    SocketDefines,
    ChangeSocketValue,
} from "./event.ts";
