import { BaseNode, BaseLine, IDentity, Socket } from "../../types/index.ts";
import { SinglerSocket, ComplexSocket } from "./propert.ts";

/** 节点基类 */
interface _BaseNode extends BaseNode {
    /** 节点标题 */
    title: string;
    /** 节点描述 */
    describe: string;
    /** 节点插槽 */
    sockets: Sockets;

    /** 出节点的线 */
    outLines: { [id: IDentity]: IDentity };
    /** 入节点的线 */
    inLines: { [id: IDentity]: IDentity };

    /** 节点函数 */
    nodeFunction(): void;
}

/** 线 */
interface _BaseLine extends BaseLine {
    /** 线头所在节点 ID */
    headNodeID: IDentity;
    /** 线尾所在节点 ID */
    tailNodeID: IDentity;
    /** 连线头所在插槽索引 */
    headSocketIndex: string;
    /** 连线尾所在插槽索引 */
    tailSocketIndex: string;
}

/** 插槽 */
type _Socket = Socket & (SinglerSocket | ComplexSocket);

/** 插槽对象 */
type Sockets = { [index: string]: _Socket };

export type {
    _BaseNode as BaseNode,
    _BaseLine as BaseLine,
    _Socket as Socket,
    Sockets,
};
