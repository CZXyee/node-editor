import { IDentity } from "../../types";

type Socket = {
    /** 插槽模式 */
    mode: "in" | "out";
    /** 值是否对外暴露 */
    isExpose: boolean;
    /** 值是否可以为未定义 */
    valueCanNull: boolean;
    /** 值标签 */
    type: string[];
};

/** 简单插槽属性 */
type SinglerSocket = Socket & {
    /** 是否为复杂值 */
    isComplex: false;
    /** 插槽值 */
    value: any;
};

/** 复杂插槽属性 */
type ComplexSocket = Socket & {
    /** 是否为复杂值 */
    isComplex: true;
    /** 插槽值对象 */
    value: { [index: IDentity]: any };
};

export type { SinglerSocket, ComplexSocket };
