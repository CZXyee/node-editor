import { SinglerSocket, ComplexSocket } from "./propert.ts";
import { IDentity } from "../../types/propert.ts";

/** 定义线 */
interface LineDefine {
    /** 线头所在节点 ID */
    headNodeID: IDentity;
    /** 线尾所在节点 ID */
    tailNodeID: IDentity;
    /** 连线头所在插槽索引 */
    headSocketIndex: string;
    /** 连线尾所在插槽索引 */
    tailSocketIndex: string;
}

/** 改变插槽值 */
type ChangeSocketValue = {
    /** 插槽所在节点id */
    nodeID: IDentity;
    /** 插槽索引 */
    socketIndex: string;
    /** 插槽值 */
    value: any;
    /** 插槽值索引 当插槽为复合类型 */
    valueIndex?: IDentity;
};

/** 定义插槽 */
type SocketDefine = SinglerSocket | ComplexSocket;

/** 定义插槽对象 */
type SocketDefines = { [index: string]: SocketDefine };

/** 定义节点 */
interface NodeDefine {
    /** 节点标题 */
    title: string;
    /** 节点描述 */
    describe: string;
    /** 节点插槽 */
    sockets: SocketDefines;

    /** 节点函数 */
    nodeFunction(): void;
}

export type { NodeDefine, LineDefine, ChangeSocketValue, SocketDefines };
