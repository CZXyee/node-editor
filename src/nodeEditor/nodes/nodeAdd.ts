import { NodeDefine } from "./interface/index.ts";

const Add: NodeDefine = {
    title: "相加",
    describe: "将两个数字相加",
    sockets: {
        numA: {
            mode: "in",
            isExpose: true,
            isComplex: false,
            valueCanNull: false,
            type: ["Number"],
            value: 0,
        },
        numB: {
            mode: "in",
            isExpose: true,
            isComplex: false,
            valueCanNull: false,
            type: ["Number"],
            value: 0,
        },
        numAdd: {
            mode: "out",
            isExpose: true,
            isComplex: false,
            valueCanNull: false,
            type: ["Number"],
            value: 0,
        },
    },

    nodeFunction: function () {
        this.sockets.numAdd.value =
            this.sockets.numA.value + this.sockets.numB.value;
    },
};

export { Add };



