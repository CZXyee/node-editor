import { Add } from "./nodeAdd.ts";
import { InputNum } from "./nodeInputNum.ts";
import { Print } from "./nodePrint.ts";
import { NodeDefine } from "./interface/index.ts";

const NodeDefines: Record<string, NodeDefine> = {
    Add,
    InputNum,
    Print,
};

export { NodeDefines };

