import { NodeDefine } from "./interface/index.ts";

const Print: NodeDefine = {
    title: "输出到控制台",
    describe: "将输入输出到控制台",
    sockets: {
        value: {
            mode: "in",
            isExpose: true,
            isComplex: false,
            valueCanNull: false,
            type: ["Number"],
            value: undefined,
        },
    },

    nodeFunction: function () {
        console.log(this.sockets.value.value);
    },
};

export { Print };





