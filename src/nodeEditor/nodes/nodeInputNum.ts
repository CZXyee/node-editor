import { NodeDefine } from "./interface/index.ts";

const InputNum: NodeDefine = {
    title: "输入数字",
    describe: "输入一个数字",
    sockets: {
        num: {
            mode: "in",
            isExpose: false,
            isComplex: false,
            valueCanNull: false,
            type: ["Number"],
            value: 0,
        },
        numOut: {
            mode: "out",
            isExpose: true,
            isComplex: false,
            valueCanNull: false,
            type: ["Number"],
            value: 0,
        },
    },

    nodeFunction: function () {
        this.sockets.numOut.value = this.sockets.num.value;
    },
};

export { InputNum };



