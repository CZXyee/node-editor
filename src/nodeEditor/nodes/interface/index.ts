import { NodeDefine } from "../../model/interface/index.ts";

interface _NodeDefine extends NodeDefine {}

export type { _NodeDefine as NodeDefine };
