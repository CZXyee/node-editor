import { IDentity } from "./propert.ts";

/** 实体*/
export interface Entity {
    /** 实体 id */
    id: IDentity;
}

/** 节点 */
export interface BaseNode extends Entity {}

/** 连线 */
export interface BaseLine extends Entity {}

/** 插槽 */
export interface Socket extends Entity {}
