/** 位置 */
export type Position = {
    x: number;
    y: number;
};

/** id */
export type IDentity = string;

/** 鼠标指针 */
export type Pointer = {
    position: Position;
};

