function processError(
    errorInfo: string,
    errorThrow: boolean
): string {
    if (errorThrow) throw new Error(errorInfo);
    //TODO 如何处理错误?
    return errorInfo;
}

export default processError;

