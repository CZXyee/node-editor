function isEmpty(obj: Object): boolean {
    let objectKeys = Object.keys(obj);
    if (objectKeys.length === 0) return true;
    return false;
}

export default isEmpty;
