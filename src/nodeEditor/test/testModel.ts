import State from "../model/state.ts";
import { NodeDefines } from "../nodes/index.ts";
import { IDentity } from "../types/propert.ts";

function test() {
    let state = new State();

    console.log("--------------------------------------------");

    let add_0 = state.addNode(NodeDefines.Add);
    let print_0 = state.addNode(NodeDefines.Print);
    let inputNum_0 = state.addNode(NodeDefines.InputNum);
    let inputNum_1 = state.addNode(NodeDefines.InputNum);

    let node_0 = state.nodeList[inputNum_0].sockets;
    console.log(node_0);
    state.changeNodeSocketValue({
        nodeID: inputNum_0,
        socketIndex: "num",
        value: 5,
    });
    console.log(node_0);
    state.changeNodeSocketValue({
        nodeID: inputNum_1,
        socketIndex: "num",
        value: 10,
    });
    console.log(node_0);

    node_0 = state.nodeList[inputNum_0].sockets;

    console.log(state.nodeList);

    let defineLine_00 = {
        headNodeID: state.nodeList[add_0].id,
        headSocketIndex: "numA",
        tailNodeID: state.nodeList[inputNum_0].id,
        tailSocketIndex: "numOut",
    };
    let line_00 = state.addLine(defineLine_00);
    state.execute(line_00 as IDentity);

    console.log(state.nodeList);

    let val = state.nodeList[inputNum_0].sockets["num"].value;
    console.log(val);

    let defineLine_1 = {
        headNodeID: state.nodeList[add_0].id,
        headSocketIndex: "numB",
        tailNodeID: state.nodeList[inputNum_1].id,
        tailSocketIndex: "numOut",
    };
    let line_1 = state.addLine(defineLine_1);
    state.execute(line_1 as IDentity);

    val = state.nodeList[inputNum_1].sockets["num"].value;
    console.log(val);

    let defineLine_2 = {
        headNodeID: state.nodeList[print_0].id,
        headSocketIndex: "value",
        tailNodeID: state.nodeList[add_0].id,
        tailSocketIndex: "numAdd",
    };
    let line_2 = state.addLine(defineLine_2);
    state.execute(line_2 as IDentity);

    val = state.nodeList[inputNum_1].sockets["num"].value;
    console.log(val);

    state.removeLine(line_2 as IDentity);

    let defineLine_3 = {
        headNodeID: state.nodeList[print_0].id,
        headSocketIndex: "value",
        tailNodeID: state.nodeList[add_0].id,
        tailSocketIndex: "numAdd",
    };
    let line_3 = state.addLine(defineLine_3);
    state.execute(line_3 as IDentity);

    console.log("--------------------------------------------");

    let add_1 = state.addNode(NodeDefines.Add);

    let defineLine_4 = {
        headNodeID: state.nodeList[add_1].id,
        headSocketIndex: "numA",
        tailNodeID: state.nodeList[inputNum_1].id,
        tailSocketIndex: "numOut",
    };
    let line_4 = state.addLine(defineLine_4);
    state.execute(line_4 as IDentity);

    let defineLine_5 = {
        headNodeID: state.nodeList[add_1].id,
        headSocketIndex: "numB",
        tailNodeID: state.nodeList[inputNum_1].id,
        tailSocketIndex: "numOut",
    };
    let line_5 = state.addLine(defineLine_5);
    state.execute(line_5 as IDentity);

    let add_2 = state.addNode(NodeDefines.Add);

    let defineLine_6 = {
        headNodeID: state.nodeList[add_2].id,
        headSocketIndex: "numA",
        tailNodeID: state.nodeList[add_1].id,
        tailSocketIndex: "numAdd",
    };
    let line_6 = state.addLine(defineLine_6);
    state.execute(line_6 as IDentity);

    let defineLine_7 = {
        headNodeID: state.nodeList[add_2].id,
        headSocketIndex: "numB",
        tailNodeID: state.nodeList[add_0].id,
        tailSocketIndex: "numAdd",
    };
    let line_7 = state.addLine(defineLine_7);
    state.execute(line_7 as IDentity);

    let print_1 = state.addNode(NodeDefines.Print);

    let defineLine_8 = {
        headNodeID: state.nodeList[print_1].id,
        headSocketIndex: "value",
        tailNodeID: state.nodeList[add_2].id,
        tailSocketIndex: "numAdd",
    };
    let line_8 = state.addLine(defineLine_8);
    state.execute(line_8 as IDentity);

    console.log("--------------------------------------------");

    let lineIndexs = state.changeNodeSocketValue({
        nodeID: inputNum_1,
        socketIndex: "num",
        value: 50,
    });

    // debugger;
    for (let lineIndex of lineIndexs) {
        state.execute(lineIndex as IDentity);
    }

    console.log("--------------------------------------------");
}

export default test;

