# 节点编辑器

#### 介绍
一套通用的前端节点编辑器，基于 canvas 

#### 软件架构
vue3 + vite + typescript

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 typescript
2.  可自定义的节点系统
